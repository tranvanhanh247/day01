<?php
require './day06/database.php';
require './students.php';
$example = queryOne($_GET['maSV']);
$lay_gender = (int) $example['gioitinh'];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Lấy dữ liệu người dùng nhập vào
    $username = $_POST["username"];
    $genderValue = $_POST["gender"];
    $departmentValue = $_POST["departmentSelect"];
    $dateOfBirth = $_POST["datebirth"];
    $address = $_POST["address"];
    $image = $_POST['img_st'];
    $image_new = $_FILES['file_image'];
    if ($image_new['size'] > 0) {
        $image = $image_new;
        move_uploaded_file($image['tmp_name'], './day06/images/' . $image['name']);
        $pathImage = $image['name'];
    } else {
        $pathImage = $image;
    }
    // var_dump($address);
    // die();

    $id_temp = $_GET['maSV'];
    $sql = "UPDATE `students` SET `hovaten`='$username',`gioitinh`='$genderValue',`ngaysinh`='$dateOfBirth',`phankhoa`='$departmentValue',`diachi`='$address',`hinhanh`='$pathImage' WHERE `id` = " . $id_temp;
    pdo_execute($sql);
    echo '<script>window.location="./index.php"</script>';
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Update</title>
    <link rel="stylesheet" href="./day06/styles.css">
</head>

<body onload="generateRadioButtons111(<?php echo $lay_gender ?>)">
    <form action="" method="post" enctype="multipart/form-data">
        <div class="form-box">
            <!-- Register Box -->
            <div class="login-box">
                <p id="error"></p>
                <!-- Input Box - Name -->
                <div class="input-box-name">
                    <label class="input-label">Họ và tên<p>*</p></label>
                    <input id="username" type="text" name="username" class="input-field-name" value="<?php extract($example);
                                                                                                        echo $hovaten ?>" required>
                </div>

                <!-- Input Box - Gender -->
                <div class="input-box-gender">
                    <label class="input-label">Giới tính<p>*</p></label>
                    <div class="radio-buttons-container" type="text" id="gioiTinhContainer"></div>
                    <!-- Trường ẩn để lưu giới tính -->
                    <input type="hidden" id="selectedGender" name="gender" value="<?php extract($example);
                                                                                    echo $gioitinh ?>">
                </div>

                <!-- Input Box - Phân khoa -->
                <div class="input-box-divide">
                    <label class="input-label">Phân khoa<p>*</p></label>
                    <input type="hidden" name="departmentSelect" value="<?php extract($example);
                                                                        echo $phankhoa; ?>">

                    <select id="departmentSelect" name="departmentSelect">
                        <option value="<?php extract($example);
                                        echo $phankhoa; ?>">
                            <?php extract($example);
                            if ($phankhoa == "MAT") {
                                echo "Khoa học máy tính";
                            } else {
                                echo "Khoa học vật liệu";
                            } ?>
                        </option>
                        <?php
                        $departments = [
                            "MAT" => "Khoa học máy tính",
                            "KDL" => "Khoa học vật liệu"
                        ];

                        foreach ($departments as $key => $value) {
                            echo "<option value='$key'>$value</option>";
                        }
                        ?>
                    </select>
                </div>

                <!-- Input Box - Ngày sinh -->
                <div class="input-box-name">
                    <label class="input-label">
                        Ngày sinh
                        <p>*</p>
                    </label>
                    <input name="datebirth" class="input-field-name" value="<?php extract($example);
                                                                            echo $ngaysinh ?>" required>
                </div>

                <!-- Input Box - Địa chỉ -->
                <div class="input-box-name" id="box-address">
                    <label class="input-label">Địa chỉ</label>
                    <input name="address" class="input-field-name" value="<?php extract($example);
                                                                            echo $diachi ?>">
                </div>

                <!-- Input Box - Hình ảnh -->
                <div class="input-box-name">
                    <label class="input-label">Hình ảnh</label>
                    <img style="margin-left: 10px" src="./day06/images/<?php
                                                                        extract($example);
                                                                        echo $hinhanh;
                                                                        ?>" width="120px" height="65px">

                    <input type="hidden" name="img_st" value="<?php
                                                                extract($example);
                                                                echo $hinhanh;
                                                                ?>">
                    <input type="file" name="file_image">
                </div>

                <!-- Submit Button -->
                <button name="xacnhan" type="submit">Xác nhận</button>
            </div>
        </div>
    </form>

</body>
<script>
    var gioiTinh = {
        0: "Nam",
        1: "Nữ",
    };

    // Hàm này được gọi khi người dùng chọn một nút radio
    function setSelectedGender(value) {
        document.getElementById("selectedGender").value = value;
    }

    // Thêm tham số vào hàm generateRadioButtons
    function generateRadioButtons111(defaultValue) {
        var radioButtons = "";
        for (var key in gioiTinh) {
            radioButtons +=
                "<input type='radio' name='gioiTinh' value='" +
                key +
                "' class='custom-radio' " +
                (key == defaultValue ? "checked" : "") +
                " onclick='setSelectedGender(this.value)'>" +
                gioiTinh[key] +
                "<br>";
        }
        document.getElementById("gioiTinhContainer").innerHTML = radioButtons;
        setSelectedGender(defaultValue);
        // Tự động "click" vào radio button mặc định
        document.getElementById("radio" + defaultValue).click();
    }
</script>

</html>