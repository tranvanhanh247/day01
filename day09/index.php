<?php
require './day06/database.php';
require './students.php';

if (isset($_GET['maSV'])) {
    deleteSV($_GET['maSV']);
    header("Location:index.php");
}

$departments = [
    "" => "All",
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu"
];

// truy vấn dữ liệu
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $key = $_POST['search'];
    $keyDPM = $_POST['tenphongban'];

    if (empty($keyDPM) && !empty($key)) {
        $queryAllStudents = searchSV($key);
        $queryCountAll = countSV($key);
    }
    if (!empty($keyDPM) && empty($key)) {
        $queryAllStudents  = searchDPM($keyDPM);
        $queryCountAll = countDPM($keyDPM);
    }
    if (!empty($keyDPM) && !empty($key)) {
        $queryAllStudents  = searchKeyAndDPM($key, $keyDPM);
        $queryCountAll = countKeyAndDPM($key, $keyDPM);
    }
    if (empty($keyDPM) && empty($key)) {
        $queryAllStudents  = queryAll();
        $queryCountAll = countAll();
    }
} else {
    $queryAllStudents  = queryAll();
    $queryCountAll = countAll();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách sinh viên</title>
    <link rel="stylesheet" href="stylesday07.css">
    <style>
        .container {
            /* bo background */
            width: 100%;
        }

        .div {
            width: 80%;
        }

        form {
            width: 100%;
        }

        table,
        td,
        th {
            border: 1px solid;
            height: 30px;
        }

        tbody td button {
            background-color: #65a952;
            margin: 5px 10px 5px 10px;
            width: 50px;
            height: 20px;
        }

        tbody td button a {
            color: white;
            text-decoration: none;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        .form-search,
        .form-key,
        .container,
        .button-search,
        .them {
            display: flex;
            justify-content: center;
            align-items: center;
            margin: 20px 0 20px 0;

        }

        input,
        .form-search select {
            margin: 0;
            padding: 3px 3px 3px 3px;
            border: 1px solid;
            width: 230px;
            height: 30px;
        }

        label {
            width: 60px;
        }

        .button-search button {
            width: 80px;
            height: 30px;
        }

        .btn-add {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .them {
            height: 25px;
            width: 70px;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="div">
            <form action="" method="post">
                <div class="form-search">
                    <label for="">Khoa</label>
                    <select name="tenphongban" type="text">
                        <?php
                        foreach ($departments as $key => $value) { ?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>

                <div class="form-key">
                    <label for="">Từ khóa</label>
                    <input name="search" type="text">
                </div>
                <div class="button-search">
                    <button name="" type="submit">Tìm kiếm</button>
                </div>
            </form>
            <div class="btn-add">
                <p>Số sinh viên tìm thấy: <?php echo $queryCountAll['soSV'] ?></p>
                <a class="them" href="./day06/register.php">Thêm</a>
            </div>
            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tên sinh viên</th>
                        <th>Khoa</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($queryAllStudents as $st) { ?>
                        <tr>
                            <td><?php extract($st);
                                echo $st['id'] ?></td>
                            <td><?php extract($st);
                                echo $st['hovaten'] ?></td>
                            <td>
                                <?php
                                extract($st);
                                foreach ($departments as $key => $value) {
                                    if ($key == $st['phankhoa']) {
                                        echo $value;
                                    } else {
                                        echo "";
                                    }
                                }
                                ?></td>
                            <td>
                                <button>
                                    <a href="index.php?maSV=<?php extract($st);
                                                            echo $st['id'] ?>" onclick="return confirm('Bạn muốn xóa sinh viên này?')">Xóa</a>
                                </button>
                                <button>
                                    <a href="update_students.php?maSV=<?php extract($st);
                                                                        echo $st['id'] ?>">Sửa</a>
                                </button>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</body>
<script src="./ajax.js"></script>

</html>