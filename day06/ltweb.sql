DROP DATABASE IF EXISTS ltweb;

CREATE DATABASE ltweb;

USE ltweb;

CREATE TABLE
    `students` (
        `id` int(20) NOT NULL,
        `hovaten` varchar(255) NOT NULL,
        `gioitinh` int(1) NOT NULL DEFAULT 0,
        `ngaysinh` VARCHAR(255) NOT NULL,
        `phankhoa` varchar(255) NOT NULL,
        `diachi` varchar(255) NOT NULL,
        `hinhanh` varchar(255) NOT NULL
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;

ALTER TABLE `students` ADD PRIMARY KEY (`id`);

ALTER TABLE `students` MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

COMMIT;