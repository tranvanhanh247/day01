<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Lấy dữ liệu người dùng nhập vào
    $username = $_POST["username"];
    $genderValue = $_POST["gender"];
    $departmentValue = $_POST["departmentSelect"];
    $dateOfBirth = $_POST["datebirth"];
    $address = $_POST["address"];
    $image = $_FILES["filename"];
    // var_dump($address);
    // die();
    move_uploaded_file($image['tmp_name'], './images/' . $image['name']);


    // Các thông tin giới tính, phòng ban tương ứng với giá trị
    $gioiTinh = [
        "0" => "Nam",
        "1" => "Nữ"
    ];

    $departments = [
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học vật liệu"
    ];
    // Lấy thông tin giới tính, phòng ban dựa trên giá trị được chọn
    $gender = isset($gioiTinh[$genderValue]) ? $gioiTinh[$genderValue] : "";
    $department = isset($departments[$departmentValue]) ? $departments[$departmentValue] : "";

    $pathImage = $image['name'];
}
?>