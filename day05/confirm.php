<?php
include './attribute.php';
require '../day06/database.php';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Đăng ký</title>
    <link rel="stylesheet" href="./styles.css">
    <style>
        .input-field-name {
            border: hidden;
        }

        input:focus {
            outline: none;
        }
    </style>
</head>

<body onload="generateRadioButtons()">
    <form action="../day06/insert.php" method="post" enctype="multipart/form-data">
        <div class="form-box">
            <!-- Register Box -->
            <div class="login-box">
                <p id="error"></p>
                <!-- Input Box - Name -->
                <div class="input-box-name">
                    <label class="input-label">Họ và tên<p>*</p></label>
                    <input name="username" class="input-field-name" value="<?php echo $username ?>" readonly>
                </div>

                <!-- Input Box - Gender -->
                <div class="input-box-gender">
                    <label class="input-label">Giới tính<p>*</p></label>
                    <input class="input-field-name" value="<?php echo $gender ?>" readonly>
                    <input name="gender" type="hidden" value="<?php echo $genderValue ?>">
                </div>

                <!-- Input Box - Phân khoa -->
                <div class="input-box-divide">
                    <label class="input-label">Phân khoa<p>*</p></label>
                    <input class="input-field-name" value="<?php echo $department ?>" readonly>
                    <input name="departmentSelect" type="hidden" value="<?php echo $departmentValue ?>">
                </div>

                <!-- Input Box - Ngày sinh -->
                <div class="input-box-name">
                    <label class="input-label">
                        Ngày sinh
                        <p>*</p>
                    </label>
                    <input name="datebirth" class="input-field-name" value="<?php echo $dateOfBirth ?>" readonly>
                </div>

                <!-- Input Box - Địa chỉ -->
                <div class="input-box-name" id="box-address">
                    <label class="input-label">Địa chỉ</label>
                    <input name="address" class="input-field-name" value="<?php echo $address ?>" readonly>
                </div>

                <!-- Input Box - Hình ảnh -->
                <div class="input-box-name">
                    <label class="input-label">Hình ảnh</label>
                    <img style="margin-left: 10px" src="./images/<?php echo $pathImage ?>" width="120px" height="65px">
                    <input name="filename" type="hidden" value="<?php echo $pathImage ?>">
                </div>

                <!-- Submit Button -->
                <button name="xacnhan" type="submit">Xác nhận</button>
            </div>
        </div>
    </form>
</body>
<script src="./script.js"></script>

</html>