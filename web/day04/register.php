<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Đăng ký</title>
    <link rel="stylesheet" href="./styles.css">
</head>

<body onload="generateRadioButtons()">
    <div class="form-box">
        <!-- Register Box -->
        <div class="login-box">
            <p id="error"></p>
            <!-- Input Box - Name -->
            <div class="input-box-name">
                <label class="input-label">Họ và tên<p>*</p></label>
                <input type="text" id="username" class="input-field-name" name="username" required>
            </div>

            <!-- Input Box - Gender -->
            <div class="input-box-gender">
                <label class="input-label">Giới tính<p>*</p></label>
                <div id="gioiTinhContainer" class="radio-buttons-container" type="text"></div>
                <!-- Trường ẩn để lưu giới tính -->
                <input type="hidden" id="selectedGender" name="gender">
            </div>

            <!-- Input Box - Phân khoa -->
            <div class="input-box-divide">
                <label class="input-label">Phân khoa<p>*</p></label>
                <select id="departmentSelect">
                    <option value="0">--Chọn phân khoa--</option>
                    <?php
                    $departments = [
                        "MAT" => "Khoa học máy tính",
                        "KDL" => "Khoa học vật liệu"
                    ];

                    foreach ($departments as $key => $value) {
                        echo "<option value='$key'>$value</option>";
                    }
                    ?>
                </select>
            </div>

            <!-- Input Box - Ngày sinh -->
            <div class="input-box-name">
                <label class="input-label">
                    Ngày sinh
                    <p>*</p>
                </label>
                <input type="text" class="input-field-name" name="datebirth" placeholder="dd/mm/yyyy" id="datebirth" required>
            </div>

            <!-- Input Box - Địa chỉ -->
            <div class="input-box-name" id="box-address">
                <label class="input-label">Địa chỉ</label>
                <input type="text" name="address" class="input-field-name" id="address"></input>
            </div>

            <!-- Submit Button -->
            <button type="submit" onclick="validate()">Đăng ký</button>
        </div>
    </div>
</body>
<script src="./script.js"></script>

</html>