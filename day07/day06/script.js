var gioiTinh = {
  0: "Nam",
  1: "Nữ",
};

function generateRadioButtons() {
  var radioButtons = "";
  for (var key in gioiTinh) {
    radioButtons +=
      "<input type='radio' name='gioiTinh' value='" +
      key +
      "' class='custom-radio' onclick='setSelectedGender(this.value)'>" +
      gioiTinh[key] +
      "<br>";
  }
  document.getElementById("gioiTinhContainer").innerHTML = radioButtons;
}

function setSelectedGender(value) {
  document.getElementById("selectedGender").value = value;
}

function validate() {
  var text = [];
  if (document.getElementById("username").value == "") {
    text.push("Hãy nhập tên .");
  }
  if (document.getElementById("selectedGender").value == "") {
    text.push("Hãy chọn giới tính .");
  }
  if (document.getElementById("departmentSelect").value == 0) {
    text.push("Hãy chọn phân khoa .");
  }

  var dateOfBirth = document.getElementById("datebirth").value;
  if (dateOfBirth == "") {
    text.push("Hãy chọn ngày sinh .");
  } else {
    var datePattern = /^\d{2}\/\d{2}\/\d{4}$/;
    if (!datePattern.test(dateOfBirth)) {
      text.push("Hãy nhập ngày sinh đúng định dạng .");
    }
  }

  var errorElement = document.getElementById("error");
  errorElement.innerHTML = text.join("<br>").replace(/,/g, "");
}
