<?php 
function queryAll()
{
    $sql = "SELECT `id`, `hovaten`, `phankhoa` FROM `students`";
    $data_kh = pdo_query($sql);
    return $data_kh;
}

function countAll() {
    $sql = "SELECT count(`id`) as `soSV` FROM `students`";
    return pdo_query_one($sql);
}

function countSV($key) {
    $sql = "SELECT count(`id`) as `soSV` FROM `students` WHERE hovaten LIKE '%$key%'";
    return pdo_query_one($sql);
}


function deleteSV($ma_SV) {
    $sql  = "DELETE FROM `students` WHERE `id` =?";
    pdo_execute($sql, $ma_SV);
}

function searchSV($key) {
    $sql = "SELECT `id`, `hovaten`, `phankhoa` FROM students WHERE hovaten LIKE '%$key%'";
    return pdo_query($sql);
}

function searchDPM($keyDPM) {
    $sql = "SELECT `id`, `hovaten`, `phankhoa` FROM students WHERE phankhoa LIKE '%$keyDPM%'";
    return pdo_query($sql);
}

function countDPM($keyDPM) {
    $sql = "SELECT count(`id`) as `soSV` FROM `students` WHERE phankhoa LIKE '%$keyDPM%'";
    return pdo_query_one($sql);
}

function searchKeyAndDPM($key, $keyDPM) {
    $sql = "SELECT `id`, `hovaten`, `phankhoa` FROM students WHERE phankhoa LIKE '%$keyDPM%' AND hovaten LIKE '%$key%'";
    return pdo_query($sql);
}

function countKeyAndDPM($key, $keyDPM) {
    $sql = "SELECT count(`id`) as `soSV` FROM `students` WHERE phankhoa LIKE '%$keyDPM%' AND hovaten LIKE '%$key%'";
    return pdo_query_one($sql);
}
