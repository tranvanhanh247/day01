<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Đăng nhập</title>
    <link rel="stylesheet" href="./styles.css">
</head>

<body>
    <!-- Form Box -->
    <div class="form-box">
        <!-- Date Box -->
        <div class="date-box">
            <?php
            // Get the current date and time
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $currentDateTime = date('H:i, l \n\g\à\y d/m/Y');
            $currentDateTime = str_replace(
                array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'),
                array('thứ 2', 'thứ 3', 'thứ 4', 'thứ 5', 'thứ 6', 'thứ 7', 'chủ nhật'),
                $currentDateTime
            );
            // Display the current date and time
            echo 'Bây giờ là: ' . $currentDateTime;
            ?>
        </div>

        <!-- Login Box -->
        <div class="login-box">
            <!-- Input Box - Username -->
            <div class="input-box">
                <label class="input-label">Tên đăng nhập</label>
                <input type="text" class="input-field" name="username" required>
            </div>

            <!-- Input Box - Password -->
            <div class="input-box">
                <label class="input-label">Mật khẩu</label>
                <input type="password" class="input-field" name="password" required>
            </div>

            <!-- Submit Button -->
            <input type="submit" value="Đăng nhập">
        </div>
    </div>
</body>

</html>