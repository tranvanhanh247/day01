<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Đăng ký</title>
    <link rel="stylesheet" href="./styles.css">
</head>

<body onload="generateRadioButtons()">
    <div class="form-box">
        <!-- Register Box -->
        <div class="login-box">
            <!-- Input Box - Name -->
            <div class="input-box-name">
                <label class="input-label">Họ và tên</label>
                <input type="text" class="input-field-name" name="username" required>
            </div>

            <!-- Input Box - Gender -->
            <div class="input-box-gender">
                <label class="input-label">Giới tính</label>
                <div id="gioiTinhContainer" class="radio-buttons-container" type="text"></div>
                <!-- Trường ẩn để lưu giới tính -->
                <input type="hidden" id="selectedGender" name="gender">
            </div>

            <!-- Input Box - Phân khoa -->
            <div class="input-box-divide">
                <label class="input-label">Phân khoa</label>
                <select id="departmentSelect">
                    <option value="">--Chọn phân khoa--</option>
                    <?php
                    $departments = [
                        "MAT" => "Khoa học máy tính",
                        "KDL" => "Khoa học vật liệu"
                    ];

                    foreach ($departments as $key => $value) {
                        echo "<option value='$key'>$value</option>";
                    }
                    ?>
                </select>
            </div>

            <!-- Submit Button -->
            <input type="submit" value="Đăng ký">
        </div>
    </div>
</body>
<script src="./script.js"></script>
</html>