var gioiTinh = {
  0: "Nam",
  1: "Nữ",
};

function generateRadioButtons() {
  var radioButtons = "";
  for (var key in gioiTinh) {
    radioButtons +=
      "<input type='radio' name='gioiTinh' value='" +
      key +
      "' class='custom-radio' onclick='setSelectedGender(this.value)'>" +
      gioiTinh[key] +
      "<br>";
  }
  document.getElementById("gioiTinhContainer").innerHTML = radioButtons;
}

function setSelectedGender(value) {
  document.getElementById("selectedGender").value = value;
}
